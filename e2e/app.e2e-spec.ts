import { WatchlistKeeperPage } from './app.po';

describe('watchlist-keeper App', () => {
  let page: WatchlistKeeperPage;

  beforeEach(() => {
    page = new WatchlistKeeperPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
