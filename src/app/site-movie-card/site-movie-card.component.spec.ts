import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteMovieCardComponent } from './site-movie-card.component';

describe('SiteMovieCardComponent', () => {
  let component: SiteMovieCardComponent;
  let fixture: ComponentFixture<SiteMovieCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteMovieCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteMovieCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
