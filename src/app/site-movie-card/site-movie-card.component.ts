import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Movie } from '../interfaces/movie'
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { ToolTipModule } from 'angular2-tooltip'

@Component({
  selector: 'app-site-movie-card',
  templateUrl: './site-movie-card.component.html',
  styleUrls: ['./site-movie-card.component.css'],
  animations: [
    trigger('saveMovieCardAnimation', [
      state('unsaved', style({
        transform: 'rotateY(0)'
      })),
      state('saved', style({
        transform: 'rotateY(179.9deg)'
      })),
      transition('unsaved => saved', animate('500ms ease-out')),
      transition('saved => unsaved', animate('500ms ease-in'))
    ])
  ]
})
export class SiteMovieCardComponent implements OnInit {

  @Input('movie') movie: Movie;

  state: string = 'unsaved';
  loading: boolean = true;

  baseUrl: string;
  smallPosterSize: string;

  showStarsBar: boolean = false;

  notificationOptions = {
    position: ["top", "right"],
    timeOut: 1000,
    lastOnBottom: true,
    animate: "fromRight"
  };

  constructor(private moviesService: MoviesService, private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.moviesService.getConfiguration().subscribe(res => {
      this.baseUrl = res.images.secure_base_url;
      this.smallPosterSize = res.images.still_sizes[2];
    });

    if (this.movie.wanting_level) {
      this.setAnimationState('saved');
    }
  }

  onLoad() {
    this.loading = false;
  }

  saveMovie(movie: Movie, isAdded: boolean, wantingLevel: number) {
    this.state = (this.state === 'unsaved' ? 'saved' : 'unsaved');
    if (isAdded) {
      movie.wanting_level = wantingLevel;

      this.moviesService.saveNewLocalMovie(movie);
      this.notificationsService.success("Saved!", "'" + movie.title + "' has been added to your watchlist with level of interest: " + wantingLevel + "/5!");
      this.showStarsBar = false;
    } else {
      this.moviesService.removeLocalMovie(movie.id);
      this.notificationsService.error("Removed!", "'" + movie.title + "' has been removed from your watchlist!");
    }
  }

  setAnimationState(state: string) {
    this.state = state;
  }


}
