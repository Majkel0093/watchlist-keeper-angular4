import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from "@angular/router";

import { SimpleNotificationsModule } from 'angular2-notifications';
import { AppComponent } from './app.component';
import { SiteHeaderComponent } from './site-header/site-header.component';
import { SiteContainerComponent } from './site-container/site-container.component';
import { SiteMovieCardComponent } from './site-movie-card/site-movie-card.component';
import { SiteMenuComponent } from './site-menu/site-menu.component';
import { SiteFooterComponent } from './site-footer/site-footer.component';

import { MoviesService } from './services/movies.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SiteLandingPageComponent } from './site-landing-page/site-landing-page.component';
import { SiteHomeComponent } from './site-home/site-home.component';
import { ToolTipModule } from 'angular2-tooltip'

const routes = [
  { path: '', component: SiteLandingPageComponent },
  { path: 'home', component: SiteHomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SiteHeaderComponent,
    SiteContainerComponent,
    SiteMovieCardComponent,
    SiteMenuComponent,
    SiteFooterComponent,
    SiteLandingPageComponent,
    SiteHomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ToolTipModule,
    SimpleNotificationsModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [MoviesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
