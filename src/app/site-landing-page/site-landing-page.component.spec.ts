import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLandingPageComponent } from './site-landing-page.component';

describe('SiteLandingPageComponent', () => {
  let component: SiteLandingPageComponent;
  let fixture: ComponentFixture<SiteLandingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteLandingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLandingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
