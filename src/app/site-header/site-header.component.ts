import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { Movie } from '../interfaces/movie'
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { SiteMovieCardComponent } from '../site-movie-card/site-movie-card.component';

@Component({
  selector: 'app-site-header',
  templateUrl: './site-header.component.html',
  styleUrls: ['./site-header.component.css']
})

export class SiteHeaderComponent implements OnInit {

  @ViewChildren(SiteMovieCardComponent) movieCards: QueryList<SiteMovieCardComponent>;

  movies: Array<Movie>;
  page: number;
  allPages: number;

  searchbarValue: string;
  currentPage: number = 1;
  showSearchResults: boolean = false;
  searchbarPlaceholder: string = "";
  searchbarPlaceholderArray: Array<string> = ['What ', 'movie ', 'are ', 'u ', 'looking ', 'for?'];

  constructor(private http: Http, private moviesService: MoviesService) { }

  ngOnInit() {
    this.animateSearchbarPlaceholder(300);
  }

  animateSearchbarPlaceholder(intervalTimeInMs: number) {
    let placeholderIndex = 0;
    let placeholderEnd = this.searchbarPlaceholderArray.length - 1;
    let intervalId;

    intervalId = setInterval(() => {
      this.searchbarPlaceholder += this.searchbarPlaceholderArray[placeholderIndex];
      if (placeholderIndex == placeholderEnd) {
        clearInterval(intervalId);
      } else {
        placeholderIndex++;
      }
    }, intervalTimeInMs);
  }

  searchbarValueChange(value: string) {
    if (value.length > 2) {
      this.showSearchResults = true;

      this.searchbarValue = value;
      this.moviesService.getMoviesByName(value, this.currentPage)
        .subscribe(res => {
          this.markSavedMovies(res.results);
          //this.movies = res.results;
          this.page = res.page;
          this.allPages = res.total_pages;
        });
    }
    else {
      this.showSearchResults = false;
    }
    this.currentPage = 1;

  }

  markSavedMovies(newMovies: Array<Movie>) {
    let saved = this.moviesService.savedMovies;
    if (saved && newMovies) {
      for (var m = 0; m < newMovies.length; m++) {
        for (var s = 0; s < saved.length; s++) {
          if (newMovies[m].id == saved[s].id) {
            newMovies[m].wanting_level = saved[s].wanting_level;
          }
        }
      }
    }
    this.movies = newMovies;
  }

  changePage(goUp: boolean) {
    if (goUp && this.currentPage < this.allPages) {
      this.currentPage++;
    } else if (!goUp && this.currentPage > 1) {
      this.currentPage--;
    }

    this.moviesService.getMoviesByName(this.searchbarValue, this.currentPage)
      .subscribe(res => {
        this.markSavedMovies(res.results);
        //this.movies = res.results;
        this.page = res.page;
        this.allPages = res.total_pages;
      });
  }

  hideSearchResults() {
    this.showSearchResults = false;
    this.searchbarValue = null;
  }
}
