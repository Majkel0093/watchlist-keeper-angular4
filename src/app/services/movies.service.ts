import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Movie } from '../interfaces/movie'

@Injectable()
export class MoviesService {

  public savedMovies: Array<Movie>;

  private apiKey: string = '9639e53ce45544e008833cd0cf5f2186';
  private storageKey: string = 'myMoviesWatchlist';

  constructor(private http: Http) {
    this.savedMovies = this.getSavedMovies();
  }

  getConfiguration() {
    let query = 'https://api.themoviedb.org/3/configuration?api_key=' + this.apiKey;
    return this.http.get(query).map(res => res.json());
  }

  getMoviesByName(name, page) {
    let query = 'https://api.themoviedb.org/3/search/movie?api_key=' + this.apiKey + '&language=en-US&query=' + name + '&page=' + page + '&include_adult=false';
    return this.http.get(query).map(res => res.json());
  }

  getSavedMovies(): Array<Movie> {
    return JSON.parse(localStorage.getItem(this.storageKey));
  }

  saveNewLocalMovie(movie: Movie) {
    let savedMovies = this.getSavedMovies();
    if (savedMovies != null && savedMovies.length > 0) {
      savedMovies.push(movie);
      localStorage.setItem(this.storageKey, JSON.stringify(savedMovies));
    } else {
      savedMovies = new Array<Movie>();
      savedMovies.push(movie);
      localStorage.setItem(this.storageKey, JSON.stringify(savedMovies));
    }
    this.savedMovies = this.getSavedMovies();
  }

  removeLocalMovie(id: number) {
    let savedMovies = this.getSavedMovies();
    if (savedMovies != null && savedMovies.length > 0) {
      savedMovies = savedMovies.filter(function (obj) {
        return obj.id != id;
      });
      localStorage.setItem(this.storageKey, JSON.stringify(savedMovies));
    }
    this.savedMovies = this.getSavedMovies();
  }
}
